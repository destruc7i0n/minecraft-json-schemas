# minecraft-json-schema
Json schemas for various minecraft files. 
Json schema information can be found at [json-schema.org](http://json-schema.org/)


## Current progress  
At the moment, the following files are supported:  

1. `advancements`
2. `pack.mcmeta`
3. `.mcmeta` animation files

### Future plans

The following files should be supported in the future

1. `recipe files`
2. `loot table files` 

The following additions are planned

1. Full titles and descriptions for all fields.
2. Implementations into different uses of schemas - especially VSCODE, which has built in support

## License  
These schemas are licensed under the UnLicense, and may be used freely without giving credit in your projects or for personal use

### Projects known to be using this